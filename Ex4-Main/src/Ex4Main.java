import java.io.*;

public class Ex4Main {
    public static void main(String[] args) throws IOException, InterruptedException {

        final String ex4PathJar = "out" + File.pathSeparator  + "artifacts" + File.pathSeparator  + "Ejercicio4_jar" + File.pathSeparator  + "Ejercicio4.jar";

        Process childProcess = new ProcessBuilder("java","-jar", ex4PathJar).start();

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(childProcess.getOutputStream());
        InputStreamReader inputStreamReader = new InputStreamReader(childProcess.getInputStream());

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in)); BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter)) {

            BufferedReader bufferedReaderLine = new BufferedReader(new InputStreamReader(System.in));

            String line;

            String exit = "finish";

            do {
                System.out.println("Write a word to translate: ");
                System.out.println("('finish' to stop)");
                line = bufferedReaderLine.readLine();

                bufferedWriter.write(line);
                bufferedWriter.newLine();
                bufferedWriter.flush();

                String result = bufferedReader.readLine();
                System.out.println(result);

            } while (line.equalsIgnoreCase(exit));

        } catch (Exception e) {
            System.err.println("Exception in Ex4" + e.getMessage());

            System.exit(1);
        } finally {
            int exit = childProcess.waitFor();

            System.out.println("Finished. Child process exit value = " + exit);
            if (exit != 0) {

            }
        }
    }

    private static void printChildErr(InputStream errorStream) {
        BufferedReader br = new BufferedReader(new InputStreamReader(errorStream));
        String line;

        try {
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
