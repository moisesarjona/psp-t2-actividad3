package es.arjona;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ex4 {
    public static void main(String[] args) {

        try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in))) {

            Diccionario diccionario = new Diccionario();
            String word;

                while ((word = bufReader.readLine()) != null) {
                    String translation;
                    translation = diccionario.getTranslation(word);

                    if (translation != null) {
                        System.out.println("Spanish: " + word + " - English: " + translation);
                    } else {
                        System.out.println("Unknown");
                    }
                }

        } catch (Exception e) {
            System.err.println("Exception in Ex4" + e.getMessage());

            System.exit(1);
        }
    }
}