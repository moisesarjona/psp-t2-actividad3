package es.arjona;

import java.util.HashMap;

public class Diccionario {

    private HashMap<String, String> content;

    public Diccionario() {
        loadContent();
    }

    private void loadContent() {
        content = new HashMap<>();
        content.put("coche", "car");
        content.put("arriba", "up");
        content.put("abajo", "down");
        content.put("casa", "house");
        content.put("negro", "black");
        content.put("blanco", "white");
        content.put("colegio", "school");
        content.put("calle", "street");
        content.put("boligrafo", "pen");
        content.put("teclado", "keyboard");
        content.put("ordenador", "computer");
        content.put("raton", "mouse");
        content.put("monitor", "monitor");
        content.put("television", "tv");
        content.put("aparato", "device");
        content.put("electrodoméstico", "appliance");
        content.put("trabajo", "work");
        content.put("cocina", "kitchen");
    }

    public String getTranslation(String word) {
        if (content.containsKey(word)) {
            return content.get(word);
        }
        return null;
    }

}